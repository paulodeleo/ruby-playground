# Ruby playground

This is a ruby 2.4 based docker container to run an included sinatra based hello world app, for development purposes. It is just a quick and easy way to have a debian based linux shell with ruby installed (so you can play on `irb`) and a sample sinatra app ready to be manually started (so you can open it in your browser and edit it later if you want to).

### Instructions:

1. Clone this repository (you need [git ](https://git-scm.com/) installed):

`git clone git@github.com:ateliware/ruby-playground.git`

2. In this repository create and run the container (you need [docker ](https://www.docker.com/community-edition) installed):

`docker-compose up -d`

3. Open a bash session on the container, like a ssh session, using this command:

`docker exec -it ruby-web bash`

4. Start the app:

`ruby sinatra_app.rb`

5. Open it on your web browser at http://localhost:3000/hello. You should see the "hello world!" message.

Now you have some options:

- To stop the app, just type `ctrl+c` on the terminal;

- To exit the container bash session, type `crtl+d` or `exit`;

- To stop the container, type `docker-compose stop`. This is the same as "hibernating" a real computer. You can "wake it up" with `docker-compose up -d` again;

- To destroy the container if you want to start from stratch again, type `docker-compose down`.